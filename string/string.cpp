// А тут реализация вашего класса

#include "string.hpp"

String::String(size_t size, const char& character) {
  CreateCStyleString(size);
  for (size_t index = 0; index < size; ++index) {
    characters_[index] = character;
  }
  size_ = size;
  capacity_ = size;
}
String::String(const char* characters) {
  size_t characters_len = std::strlen(characters);
  SetSizeAndCapacityAndCharacters(characters_len, characters_len, characters);
}
String::String(const String& string_to_copy) {
  SetSizeAndCapacityAndCharacters(string_to_copy.size_,
                                  string_to_copy.capacity_,
                                  string_to_copy.characters_);
}

void String::PushBack(const char& character) {
  ChangeSize(1);
  characters_[size_ - 1] = character;
}

void String::Resize(size_t new_size) {
  if (new_size > capacity_) {
    SetSizeAndCapacityAndCharacters(new_size, new_size, characters_);
  }
  const int kDelta = static_cast<int>(new_size) - static_cast<int>(size_);
  ChangeSize(kDelta);
}
void String::Resize(size_t new_size, const char& character) {
  size_t previous_size = size_;
  Resize(new_size);
  for (size_t index = previous_size; index < size_; ++index) {
    characters_[index] = character;
  }
}
void String::Reserve(size_t new_cap) {
  if (new_cap > capacity_) {
    SetSizeAndCapacityAndCharacters(size_, new_cap, characters_);
  }
}
void String::ShrinkToFit() {
  if (capacity_ > size_) {
    SetSizeAndCapacityAndCharacters(size_, size_, characters_);
  }
}

void String::Swap(String& other) {
  std::swap(this->characters_, other.characters_);
  std::swap(this->size_, other.size_);
  std::swap(this->capacity_, other.capacity_);
}

std::vector<String> String::Split(const String& delim) {
  std::vector<String> strings;
  size_t index = 0;
  size_t start_index = 0;
  size_t delim_index = 0;
  while (index < size_) {
    index += 1;
    if (characters_[index - 1] != delim.characters_[delim_index]) {
      delim_index = 0;
      continue;
    }
    delim_index += 1;
    if (delim_index == delim.size_) {
      String new_string;
      new_string.CreateCharacters(index - start_index - delim.size_,
                                  characters_ + start_index);
      strings.push_back(String(new_string));
      start_index = index;
      delim_index = 0;
    }
  }
  String new_string;
  new_string.CreateCharacters(index - start_index, characters_ + start_index);
  strings.push_back(new_string);
  return strings;
}
String String::Join(const std::vector<String>& strings) {
  if (strings.empty()) {
    return String("");
  }
  size_t new_size = size_ * (strings.size() - 1);
  for (size_t index = 0; index < strings.size(); ++index) {
    new_size += strings[index].size_;
  }
  String new_string(new_size, ' ');
  size_t offset = 0;
  for (size_t index = 0; index < strings.size(); ++index) {
    std::memcpy(new_string.Data() + offset, strings[index].characters_,
                strings[index].size_);
    offset += strings[index].size_;
    if (index < strings.size() - 1) {
      std::memcpy(new_string.Data() + offset, characters_, size_);
    }
    offset += size_;
  }
  return new_string;
}

const char& String::operator[](size_t index) const {
  return characters_[index];
}

String& String::operator=(const String& string_to_copy) {
  if (&string_to_copy == this) {
    return *this;
  }
  SetSizeAndCapacityAndCharacters(string_to_copy.size_,
                                  string_to_copy.capacity_,
                                  string_to_copy.characters_);
  return *this;
}
String& String::operator=(const char* characters) {
  size_t new_size = static_cast<int>(strlen(characters));
  SetSizeAndCapacityAndCharacters(new_size, CalculateCapacity(new_size),
                                  characters);
  return *this;
}
String& String::operator+=(const String& string_to_plus) {
  size_t new_size = Size() + string_to_plus.Size();
  String new_string(new_size, ' ');
  if (Data() != nullptr) {
    memcpy(new_string.Data(), Data(), Size());
  }
  if (string_to_plus.Data() != nullptr) {
    memcpy(new_string.Data() + Size(), string_to_plus.Data(),
           string_to_plus.Size());
  }
  *this = new_string;
  return *this;
}
String& String::operator*=(size_t number) {
  size_t previous_size = Size();
  size_t new_size = previous_size * number;
  String new_string(new_size, ' ');
  for (size_t index = 0; index < number; ++index) {
    std::memcpy(new_string.Data() + (previous_size * index), characters_,
                previous_size);
  }
  *this = new_string;
  return *this;
}
String String::operator*(size_t number) const {
  String new_string = *this;
  new_string *= number;
  return new_string;
}

void String::CreateCStyleString(size_t c_style_string_len) {
  characters_ = new char[c_style_string_len + 1];
  characters_[c_style_string_len] = '\0';
}
size_t String::CalculateCapacity(size_t size) {
  int count = 0;
  size_t temp_size = size;
  while (temp_size / 2 > 0) {
    temp_size >>= 1;
    count += 1;
  }
  return (1 << count) * 2;
}
void String::CloseCString() {
  if (size_ < capacity_) {
    characters_[size_] = '\0';
  }
}
void String::ChangeSize(int delta) {
  if (delta > 0) {
    size_t new_size = size_ + delta;
    size_t new_capacity = capacity_;
    if (new_size > capacity_) {
      new_capacity *= 2;
      if (capacity_ == 0) {
        new_capacity = 1;
      }
    }
    SetSizeAndCapacityAndCharacters(new_size, new_capacity, characters_);
  } else if (delta < 0) {
    if (static_cast<int>(size_) + delta < 0) {
      return;
    }
    size_ += delta;
  }
  CloseCString();
}
void String::SetSizeAndCapacityAndCharacters(size_t new_size,
                                             size_t new_capacity,
                                             const char* new_characters) {
  if (new_characters == nullptr) {
    new_characters = characters_;
  }
  capacity_ = new_capacity;
  size_ = new_size;
  MemcpyToCharacters(new_characters);
}
void String::MemcpyToCharacters(const char* c_string_to_copy) {
  char* previous_characters_pointer = characters_;
  CreateCStyleString(capacity_);
  if (c_string_to_copy != nullptr && size_ >= std::strlen(c_string_to_copy)) {
    std::memcpy(characters_, c_string_to_copy, std::strlen(c_string_to_copy));
    delete[] previous_characters_pointer;
  }
  CloseCString();
}
void String::CreateCharacters(size_t new_string_size,
                              const char* c_string_to_copy) {
  CreateCStyleString(new_string_size);
  SetSizeAndCapacityAndCharacters(
      new_string_size, CalculateCapacity(new_string_size), characters_);
  std::memcpy(characters_, c_string_to_copy, new_string_size);
}

std::ostream& operator<<(std::ostream& ostream, const String& string) {
  ostream << string.Data();
  return ostream;
}
std::istream& operator>>(std::istream& istream, String& string) {
  size_t index = 0;
  char character = static_cast<char>(istream.get());
  while ((std::isspace(character) == 0 && istream) || index == 0) {
    if (std::isspace(character) != 0) {
      character = static_cast<char>(istream.get());
      continue;
    }
    string.PushBack(character);
    character = static_cast<char>(istream.get());
    index += 1;
  }
  return istream;
}

bool operator<(const String& lhs, const String& rhs) {
  size_t common_index = 0;
  while (common_index < (lhs.Size() - 1) && common_index < (rhs.Size() - 1)) {
    if (lhs[common_index] < rhs[common_index]) {
      return true;
    }
    if (lhs[common_index] > rhs[common_index]) {
      return false;
    }
    common_index += 1;
  }
  return common_index >= (lhs.Size() - 1);
}
bool operator==(const String& lhs, const String& rhs) {
  if (lhs.Size() == rhs.Size()) {
    size_t common_index = 0;
    if (lhs.Size() == 0 && rhs.Size() == 0) {
      return true;
    }
    while (common_index < (lhs.Size() - 1) && common_index < (rhs.Size() - 1)) {
      if (lhs[common_index] != rhs[common_index]) {
        return false;
      }
      common_index += 1;
    }
    return true;
  }
  return false;
}
bool operator>(const String& lhs, const String& rhs) { return rhs < lhs; }
bool operator<=(const String& lhs, const String& rhs) { return !(lhs > rhs); }
bool operator>=(const String& lhs, const String& rhs) { return !(lhs < rhs); }
bool operator!=(const String& lhs, const String& rhs) { return !(lhs == rhs); }

String operator+(const String& lhs, const String& rhs) {
  String new_string = lhs;
  new_string += rhs;
  return new_string;
}
