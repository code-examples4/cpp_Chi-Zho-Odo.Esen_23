#pragma once

// Тут объявление вашего класса

#include <cstring>
#include <iostream>
#include <vector>

class String {
 public:
  String() = default;
  String(size_t size, const char& character);
  String(const char* characters);
  String(const String& string_to_copy);
  ~String() { delete[] characters_; }

  void Clear() { size_ = 0; }

  void PushBack(const char& character);
  void PopBack() { ChangeSize(-1); }

  void Resize(size_t new_size);
  void Resize(size_t new_size, const char& character);
  void Reserve(size_t new_cap);
  void ShrinkToFit();

  void Swap(String& other);

  char& Front() { return characters_[0]; }
  const char& Front() const { return characters_[0]; }

  char& Back() { return characters_[size_ - 1]; }
  const char& Back() const { return characters_[size_ - 1]; }

  bool Empty() const { return size_ == 0; }
  size_t Size() const { return size_; }
  size_t Capacity() const { return capacity_; }

  char* Data() { return characters_; }
  const char* Data() const { return characters_; }

  std::vector<String> Split(const String& delim = " ");
  String Join(const std::vector<String>& strings);

  char& operator[](const size_t kIndex) { return characters_[kIndex]; }
  const char& operator[](size_t index) const;

  String& operator=(const String& string_to_copy);
  String& operator=(const char* characters);
  String& operator+=(const String& string_to_plus);
  String& operator*=(size_t number);
  String operator*(size_t number) const;

  void CreateCStyleString(size_t c_style_string_len);
  void CreateCharacters(size_t new_string_size, const char* c_string_to_copy);

 private:
  char* characters_ = nullptr;
  size_t size_ = 0;
  size_t capacity_ = 0;

  static size_t CalculateCapacity(size_t size);
  void CloseCString();
  void ChangeSize(int delta);
  void SetSizeAndCapacityAndCharacters(size_t new_size, size_t new_capacity,
                                       const char* new_characters = nullptr);
  void MemcpyToCharacters(const char* c_string_to_copy);
};

std::ostream& operator<<(std::ostream& ostream, const String& string);
std::istream& operator>>(std::istream& istream, String& string);

bool operator<(const String& lhs, const String& rhs);
bool operator==(const String& lhs, const String& rhs);
bool operator>(const String& lhs, const String& rhs);
bool operator<=(const String& lhs, const String& rhs);
bool operator>=(const String& lhs, const String& rhs);
bool operator!=(const String& lhs, const String& rhs);

String operator+(const String& lhs, const String& rhs);
