#include <cstddef>
#include <vector>

class RingBuffer {
 public:
  explicit RingBuffer(size_t capacity)
      : capacity_(capacity), ring_buffer_(capacity) {}

  size_t Size() const { return abstract_size_; }

  bool Empty() const { return abstract_size_ == 0; }

  bool TryPush(int element) {
    if (Size() == capacity_) {
      return false;
    }
    ring_buffer_[(front_ + abstract_size_) % capacity_] = element;
    abstract_size_ += 1;
    return true;
  }

  bool TryPop(int* element) {
    if (Size() == 0) {
      return false;
    }
    *element = ring_buffer_[front_];
    front_ = (front_ + 1) % capacity_;
    abstract_size_ -= 1;
    return true;
  }

 private:
  size_t front_ = 0;
  size_t abstract_size_ = 0;
  size_t capacity_;
  std::vector<int> ring_buffer_;
};
