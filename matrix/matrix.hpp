#pragma once

#include <cstdio>
#include <iostream>
#include <vector>

template <size_t N, size_t M, typename T = int64_t>
class Matrix {
 public:
  Matrix() : data_(std::vector<std::vector<T>>(N, std::vector<T>(M))) {}
  Matrix(const std::vector<std::vector<T>>& vector_to_copy)
      : data_(vector_to_copy) {}
  Matrix(const T& elem);

  Matrix<M, N, T> Transposed();

  T Trace();

  Matrix& operator+=(const Matrix<N, M, T>& matrix_to_plus);
  Matrix& operator-=(const Matrix<N, M, T>& matrix_to_minus);

  T& operator()(size_t row, size_t column) { return data_[row][column]; }
  const T& operator()(size_t row, size_t column) const {
    return data_[row][column];
  }

 private:
  std::vector<std::vector<T>> data_;
};

template <size_t N, size_t M, typename T>
Matrix<N, M, T>& Matrix<N, M, T>::operator-=(
    const Matrix<N, M, T>& matrix_to_minus) {
  for (size_t row = 0; row < N; ++row) {
    for (size_t column = 0; column < M; ++column) {
      data_[row][column] -= matrix_to_minus(row, column);
    }
  }
  return *this;
}

template <size_t N, size_t M, typename T>
Matrix<N, M, T>& Matrix<N, M, T>::operator+=(
    const Matrix<N, M, T>& matrix_to_plus) {
  for (size_t row = 0; row < N; ++row) {
    for (size_t column = 0; column < M; ++column) {
      data_[row][column] += matrix_to_plus(row, column);
    }
  }
  return *this;
}

template <size_t N, size_t M, typename T>
T Matrix<N, M, T>::Trace() {
  static_assert(N == M, "The lecturer said it's forbidden");
  T sum;
  for (size_t index = 0; index < N; ++index) {
    sum += data_[index][index];
  }
  return sum;
}

template <size_t N, size_t M, typename T>
Matrix<M, N, T> Matrix<N, M, T>::Transposed() {
  std::vector<std::vector<T>> new_data(M, std::vector<T>(N));
  for (size_t row = 0; row < N; ++row) {
    for (size_t column = 0; column < M; ++column) {
      new_data[column][row] = data_[row][column];
    }
  }
  Matrix<M, N, T> new_matrix(new_data);
  return new_matrix;
}

template <size_t N, size_t M, typename T>
Matrix<N, M, T>::Matrix(const T& elem)
    : data_(std::vector<std::vector<T>>(N, std::vector<T>(M))) {
  for (size_t row = 0; row < N; ++row) {
    for (size_t column = 0; column < M; ++column) {
      data_[row][column] = elem;
    }
  }
}

template <size_t N1, size_t M1, size_t M2, typename T1>
Matrix<N1, M2, T1> operator*(const Matrix<N1, M1, T1>& lhs,
                             const Matrix<M1, M2, T1>& rhs) {
  Matrix<N1, M2, T1> new_matrix;
  for (size_t new_matrix_row = 0; new_matrix_row < N1; ++new_matrix_row) {
    for (size_t new_matrix_column = 0; new_matrix_column < M2;
         ++new_matrix_column) {
      new_matrix(new_matrix_row, new_matrix_column) =
          lhs(new_matrix_row, 0) * rhs(0, new_matrix_column);
      for (size_t index = 1; index < M1; ++index) {
        new_matrix(new_matrix_row, new_matrix_column) +=
            lhs(new_matrix_row, index) * rhs(index, new_matrix_column);
      }
    }
  }
  return new_matrix;
}

template <size_t N1, size_t M1, typename T1>
Matrix<N1, M1, T1> operator*(const Matrix<N1, M1, T1>& lhs, const T1& elem) {
  Matrix<N1, M1, T1> new_matrix = lhs;
  for (size_t row = 0; row < N1; ++row) {
    for (size_t column = 0; column < M1; ++column) {
      new_matrix(row, column) *= elem;
    }
  }
  return new_matrix;
}

template <size_t N1, size_t M1, typename T1>
Matrix<N1, M1, T1> operator+(const Matrix<N1, M1, T1>& lhs,
                             const Matrix<N1, M1, T1>& rhs) {
  Matrix<N1, M1, T1> new_matrix(lhs);
  new_matrix += rhs;
  return new_matrix;
}

template <size_t N1, size_t M1, typename T1>
Matrix<N1, M1, T1> operator-(const Matrix<N1, M1, T1>& lhs,
                             const Matrix<N1, M1, T1>& rhs) {
  Matrix<N1, M1, T1> new_matrix(lhs);
  new_matrix -= rhs;
  return new_matrix;
}

template <size_t N1, size_t M1, typename T1>
bool operator==(const Matrix<N1, M1, T1>& lhs, const Matrix<N1, M1, T1>& rhs) {
  for (size_t row = 0; row < N1; ++row) {
    for (size_t column = 0; column < M1; ++column) {
      if (lhs(row, column) != rhs(row, column)) {
        return false;
      }
    }
  }
  return true;
}
