//
// Created by esen on 24.02.24.
//

#ifndef TESTING_REPO_CPP_DEQUE_HPP
#define TESTING_REPO_CPP_DEQUE_HPP

#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

template <typename T>
class Deque {
 public:
  template <bool IsConst = false>
  class Iterator;

  using iterator = Iterator<false>;
  using const_iterator = Iterator<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  Deque() = default;
  Deque(const size_t& count, const T& value);
  Deque(const size_t& count);
  Deque(const Deque<T>& deque_to_copy);
  ~Deque();

  iterator begin();
  iterator end();
  iterator begin() const;
  iterator end() const;

  reverse_iterator rbegin();
  reverse_iterator rend();
  const_reverse_iterator rbegin() const;
  const_reverse_iterator rend() const;

  const_iterator cbegin() const;
  const_iterator cend() const;

  const_reverse_iterator crbegin() const;
  const_reverse_iterator crend() const;

  size_t size() const;
  bool empty();
  void push_back(const T& new_element);
  void push_front(const T& new_element);
  void pop_back();
  void pop_front();
  void insert(iterator element_iterator, const T& element);
  void erase(iterator element_iterator);
  T& at(const size_t& index);
  const T& at(const size_t& index) const;

  Deque& operator=(const Deque& rhs);
  T& operator[](const size_t& index);
  const T& operator[](const size_t& index) const;

 private:
  void init_values();
  void push(const std::pair<size_t, size_t>& indexes, const T& new_element);
  void push(const iterator& iterator_new_element, const T& new_element);
  void destruct_elements(std::pair<size_t, size_t> start_indexes,
                         const std::pair<size_t, size_t>& end_indexes);
  void destruct_all_elements();
  void delete_buckets(const size_t& start_bucket_index,
                      const size_t& end_bucket_index);
  void destroy();
  void reserve(const size_t& new_buckets_count);
  void reserve_if_front_necessary();
  void reserve_if_back_necessary();

  void move_forward(std::pair<size_t, size_t>& indexes);
  void move_back(std::pair<size_t, size_t>& indexes);

  void move_begin_forward();
  void move_begin_back();
  void move_end_forward();
  void move_end_back();

  static const size_t kBucketSize = 10;
  T** buckets_ = nullptr;
  size_t head_bucket_index_ = 0;
  std::pair<size_t, size_t> begin_{0, 0};
  std::pair<size_t, size_t> end_{0, 0};
  size_t size_ = 0;
  size_t capacity_ = 0;
};

template <typename T>
template <bool IsConst>
class Deque<T>::Iterator {
 public:
  using iterator_category = std::random_access_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using cond_type = std::conditional_t<IsConst, const T, T>;
  using value_type = cond_type;
  using pointer = cond_type*;
  using reference = cond_type&;

  Iterator(T** bucket, T* element)
      : bucket_pointer_(bucket), element_pointer_(element) {
    if (bucket != nullptr) {
      bucket_begin_ = *bucket;
      bucket_end_ = *bucket + kBucketSize;
    } else {
      bucket_begin_ = nullptr;
      bucket_end_ = nullptr;
    }
  }
  Iterator(const Iterator&) = default;

  operator Iterator<true>() const {
    return Iterator<true>(bucket_pointer_, element_pointer_);
  }

  difference_type operator-(const Iterator& rhs) const {
    return (this->bucket_pointer_ - rhs.bucket_pointer_) * kBucketSize +
           ((this->element_pointer_ - this->bucket_begin_) -
            (rhs.element_pointer_ - rhs.bucket_begin_));
  }
  bool operator==(const Iterator& rhs) const {
    return this->element_pointer_ == rhs.element_pointer_;
  }
  bool operator!=(const Iterator& rhs) const {
    return this->element_pointer_ != rhs.element_pointer_;
  }
  Iterator& operator++() {
    move_forward(1);
    return *this;
  }
  Iterator& operator--() {
    move_back(1);
    return *this;
  }
  Iterator operator--(int) {
    Iterator tmp = *this;
    move_back(1);
    return tmp;
  }
  Iterator operator++(int) {
    Iterator tmp = *this;
    move_forward(1);
    return tmp;
  }
  Iterator& operator+=(const difference_type& n) {
    move_forward(n);
    return *this;
  }
  Iterator& operator-=(const difference_type& n) {
    move_back(n);
    return *this;
  }
  Iterator operator+(const difference_type& n) const {
    Iterator tmp = *this;
    tmp.move_forward(n);
    return tmp;
  }
  Iterator operator-(const difference_type& n) const {
    Iterator tmp = *this;
    tmp.move_back(n);
    return tmp;
  }

  reference operator*() const { return *element_pointer_; }
  pointer operator->() const { return element_pointer_; }
  Iterator& operator=(const Iterator&) = default;
  bool operator<(const Iterator& rhs) { return *this - rhs < 0; }
  bool operator>(const Iterator& rhs) { return *this - rhs > 0; }
  bool operator<=(const Iterator& rhs) { return *this - rhs <= 0; }
  bool operator>=(const Iterator& rhs) { return *this - rhs >= 0; }

  bool is_null() {
    return bucket_pointer_ == nullptr && bucket_begin_ == nullptr &&
           bucket_end_ == nullptr && element_pointer_ == nullptr;
  }

 private:
  void move_forward(const size_t& delta) {
    if (delta == 0) {
      return;
    }
    size_t bucket_delta =
        (delta + element_pointer_ - bucket_begin_) / kBucketSize;
    size_t element_delta =
        (delta + element_pointer_ - bucket_begin_) % kBucketSize;
    bucket_pointer_ += bucket_delta;
    bucket_begin_ = *bucket_pointer_;
    bucket_end_ = *bucket_pointer_ + kBucketSize;
    element_pointer_ = bucket_begin_ + element_delta;
  }
  void move_back(const int32_t& delta) {
    if (delta == 0) {
      return;
    }
    size_t bucket_delta =
        ((bucket_end_ - 1) - (element_pointer_ - delta)) / kBucketSize;
    size_t element_delta =
        ((bucket_end_ - 1) - (element_pointer_ - delta)) % kBucketSize;
    bucket_pointer_ -= bucket_delta;
    bucket_begin_ = *bucket_pointer_;
    bucket_end_ = *bucket_pointer_ + kBucketSize;
    element_pointer_ = (bucket_end_ - 1) - element_delta;
  }

  T** bucket_pointer_ = nullptr;
  T* bucket_begin_ = nullptr;
  T* bucket_end_ = nullptr;
  T* element_pointer_ = nullptr;
};

template <typename T>
Deque<T>::Deque(const size_t& count, const T& value) {
  reserve((count + (kBucketSize - 1)) / kBucketSize);
  try {
    for (size_t last_element_index = 0; last_element_index < count;
         ++last_element_index) {
      push_back(value);
    }
  } catch (...) {
    destroy();

    init_values();
    throw;
  }
}
template <typename T>
Deque<T>::Deque(const size_t& count) : Deque(count, T()) {}
template <typename T>
Deque<T>::Deque(const Deque<T>& deque_to_copy) {
  reserve(deque_to_copy.size());
  if (deque_to_copy.size() == 0) {
    return;
  }

  iterator element_iterator = begin();
  try {
    for (iterator index_iterator = deque_to_copy.begin();
         index_iterator < deque_to_copy.end();
         ++index_iterator, ++element_iterator) {
      new (element_iterator.operator->()) T(*index_iterator);
    }
  } catch (...) {
    destroy();

    init_values();
    throw;
  }
  head_bucket_index_ = deque_to_copy.head_bucket_index_;
  begin_ = deque_to_copy.begin_;
  end_ = deque_to_copy.end_;
  capacity_ = deque_to_copy.capacity_;
}
template <typename T>
Deque<T>::~Deque() {
  destroy();
}
template <typename T>
Deque<T>::iterator Deque<T>::begin() {
  if (capacity_ / kBucketSize <= begin_.first) {
    return iterator(nullptr, nullptr);
  }
  return iterator(buckets_ + begin_.first,
                  buckets_[begin_.first] + begin_.second);
}
template <typename T>
Deque<T>::iterator Deque<T>::end() {
  if (capacity_ / kBucketSize <= end_.first) {
    return iterator(nullptr, nullptr);
  }
  return iterator(buckets_ + end_.first, buckets_[end_.first] + end_.second);
}
template <typename T>
Deque<T>::iterator Deque<T>::begin() const {
  if (capacity_ / kBucketSize <= begin_.first) {
    return iterator(nullptr, nullptr);
  }
  return iterator(buckets_ + begin_.first,
                  buckets_[begin_.first] + begin_.second);
}
template <typename T>
Deque<T>::iterator Deque<T>::end() const {
  if (capacity_ / kBucketSize <= end_.first) {
    return iterator(nullptr, nullptr);
  }
  return iterator(buckets_ + end_.first, buckets_[end_.first] + end_.second);
}
template <typename T>
Deque<T>::reverse_iterator Deque<T>::rbegin() {
  return reverse_iterator(end());
}
template <typename T>
Deque<T>::reverse_iterator Deque<T>::rend() {
  return reverse_iterator(begin());
}
template <typename T>
Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
  return const_reverse_iterator(end());
}
template <typename T>
Deque<T>::const_reverse_iterator Deque<T>::rend() const {
  return const_reverse_iterator(begin());
}
template <typename T>
Deque<T>::const_iterator Deque<T>::cbegin() const {
  if (capacity_ / kBucketSize <= begin_.first) {
    return iterator(nullptr, nullptr);
  }
  return iterator(buckets_ + begin_.first,
                  buckets_[begin_.first] + begin_.second);
}
template <typename T>
Deque<T>::const_iterator Deque<T>::cend() const {
  if (capacity_ / kBucketSize <= end_.first) {
    return iterator(nullptr, nullptr);
  }
  return iterator(buckets_ + end_.first, buckets_[end_.first] + end_.second);
}
template <typename T>
Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
  return const_reverse_iterator(end());
}
template <typename T>
Deque<T>::const_reverse_iterator Deque<T>::crend() const {
  return const_reverse_iterator(begin());
}
template <typename T>
size_t Deque<T>::size() const {
  return size_;
}
template <typename T>
bool Deque<T>::empty() {
  return begin_ == end_;
}
template <typename T>
void Deque<T>::push_back(const T& new_element) {
  reserve_if_back_necessary();
  push(end_, new_element);
  move_end_forward();
  ++size_;
}
template <typename T>
void Deque<T>::push_front(const T& new_element) {
  reserve_if_front_necessary();
  std::pair<size_t, size_t> element_indexes = begin_;
  move_back(element_indexes);
  push(element_indexes, new_element);
  move_begin_back();
  ++size_;
}
template <typename T>
void Deque<T>::pop_back() {
  std::pair<size_t, size_t> element_indexes = end_;
  move_back(element_indexes);
  buckets_[element_indexes.first][element_indexes.second].~T();
  move_end_back();
  --size_;
}
template <typename T>
void Deque<T>::pop_front() {
  buckets_[begin_.first][begin_.second].~T();
  move_begin_forward();
  --size_;
}
template <typename T>
void Deque<T>::insert(Deque::iterator element_iterator, const T& element) {
  if (element_iterator != end() && !element_iterator.is_null()) {
    reserve_if_front_necessary();
    iterator current_element_iterator = end();
    iterator previous_element_iterator = current_element_iterator - 1;
    push(current_element_iterator, *previous_element_iterator);
    --current_element_iterator;
    --previous_element_iterator;
    for (; current_element_iterator > element_iterator;
         --current_element_iterator, --previous_element_iterator) {
      *current_element_iterator = *previous_element_iterator;
    }
    *element_iterator = element;
    move_end_forward();
    ++size_;
  } else {
    push_back(element);
  }
}
template <typename T>
void Deque<T>::erase(Deque::iterator element_iterator) {
  iterator next_element_iterator = element_iterator + 1;
  for (; element_iterator < --end();
       ++element_iterator, ++next_element_iterator) {
    *element_iterator = *next_element_iterator;
  }
  move_end_back();
  --size_;
}
template <typename T>
T& Deque<T>::at(const size_t& index) {
  if (index >= (((end_.first * kBucketSize) + end_.second) -
                ((begin_.first * kBucketSize) + begin_.second))) {
    throw std::out_of_range("out of range");
  }
  return buckets_[begin_.first + (index / kBucketSize)]
                 [begin_.second + (index % kBucketSize)];
}
template <typename T>
const T& Deque<T>::at(const size_t& index) const {
  if (index > (((end_.first * kBucketSize) + end_.second) -
               ((begin_.first * kBucketSize) + begin_.second))) {
    throw std::out_of_range("out of range");
  }
  return buckets_[begin_.first + (index / kBucketSize)]
                 [begin_.second + (index % kBucketSize)];
}
template <typename T>
Deque<T>& Deque<T>::operator=(const Deque& rhs) {
  destruct_all_elements();
  try {
    for (const T& element : rhs) {
      push_back(element);
    }
  } catch (...) {
    destroy();

    init_values();
    throw;
  }
  return *this;
}
template <typename T>
T& Deque<T>::operator[](const size_t& index) {
  return buckets_[begin_.first + (index / kBucketSize)]
                 [begin_.second + (index % kBucketSize)];
}
template <typename T>
const T& Deque<T>::operator[](const size_t& index) const {
  return buckets_[begin_.first + (index / kBucketSize)]
                 [begin_.second + (index % kBucketSize)];
}
template <typename T>
void Deque<T>::init_values() {
  buckets_ = nullptr;
  head_bucket_index_ = 0;
  begin_ = {0, 0};
  end_ = {0, 0};
  size_ = 0;
  capacity_ = 0;
}
template <typename T>
void Deque<T>::push(const std::pair<size_t, size_t>& indexes,
                    const T& new_element) {
  try {
    new (buckets_[indexes.first] + indexes.second) T(new_element);
  } catch (...) {
    buckets_[indexes.first][indexes.second].~T();
    throw;
  }
}
template <typename T>
void Deque<T>::push(const Deque::iterator& iterator_new_element,
                    const T& new_element) {
  try {
    new (iterator_new_element.operator->()) T(new_element);
  } catch (...) {
    (*iterator_new_element).~T();
    throw;
  }
}
template <typename T>
void Deque<T>::destruct_elements(std::pair<size_t, size_t> start_indexes,
                                 const std::pair<size_t, size_t>& end_indexes) {
  for (; start_indexes < end_indexes;) {
    buckets_[start_indexes.first][start_indexes.second].~T();
    move_forward(start_indexes);
  }
}
template <typename T>
void Deque<T>::destruct_all_elements() {
  destruct_elements(begin_, end_);
  begin_.first = head_bucket_index_;
  begin_.second = 0;
  end_ = begin_;
  size_ = 0;
}
template <typename T>
void Deque<T>::delete_buckets(const size_t& start_bucket_index,
                              const size_t& end_bucket_index) {
  for (size_t bucket_index = start_bucket_index;
       bucket_index < end_bucket_index; ++bucket_index) {
    delete[] reinterpret_cast<std::byte*>(buckets_[bucket_index]);
  }
}
template <typename T>
void Deque<T>::destroy() {
  if (capacity_ == 0) {
    return;
  }
  if (size_ > 0) {
    destruct_all_elements();
  }
  delete_buckets(0, capacity_ / kBucketSize);
  delete[] buckets_;
}
template <typename T>
void Deque<T>::reserve(const size_t& new_buckets_count) {
  if (new_buckets_count <= capacity_ / kBucketSize) {
    return;
  }

  T** new_buckets = nullptr;
  try {
    new_buckets = new T*[new_buckets_count];
  } catch (...) {
    delete[] new_buckets;
    throw;
  }
  size_t new_head_bucket_index = new_buckets_count / 2;
  size_t new_begin_bucket_index =
      new_head_bucket_index - (end_.first - begin_.first) / 2;
  size_t new_end_bucket_index =
      new_head_bucket_index + (end_.first - begin_.first + 1) / 2;

  for (size_t index = 0; index < capacity_ / kBucketSize; ++index) {
    new_buckets[new_begin_bucket_index - begin_.first + index] =
        buckets_[index];
  }
  for (size_t index = 0; index < new_begin_bucket_index - begin_.first;
       ++index) {
    try {
      new_buckets[index] =
          reinterpret_cast<T*>(new std::byte[kBucketSize * sizeof(T)]);
    } catch (...) {
      delete[] reinterpret_cast<std::byte*>(new_buckets[index]);
      throw;
    }
  }
  for (size_t index =
           new_begin_bucket_index - begin_.first + capacity_ / kBucketSize;
       index < new_buckets_count; ++index) {
    try {
      new_buckets[index] =
          reinterpret_cast<T*>(new std::byte[kBucketSize * sizeof(T)]);
    } catch (...) {
      delete[] reinterpret_cast<std::byte*>(new_buckets[index]);
      throw;
    }
  }

  delete[] buckets_;
  buckets_ = new_buckets;
  head_bucket_index_ = new_head_bucket_index;
  begin_.first = new_begin_bucket_index;
  end_.first = new_end_bucket_index;
  capacity_ = new_buckets_count * kBucketSize;
}
template <typename T>
void Deque<T>::reserve_if_back_necessary() {
  if (capacity_ == 0) {
    reserve(2);
  } else if (end_.first == ((capacity_ / kBucketSize) - 1) &&
             end_.second == (kBucketSize - 1)) {
    reserve(capacity_ / kBucketSize * 3);
  }
}
template <typename T>
void Deque<T>::reserve_if_front_necessary() {
  if (begin_.first == 0 && begin_.second == 0) {
    if (capacity_ == 0) {
      reserve(2);
    } else {
      reserve(capacity_ / kBucketSize * 3);
    }
  }
}
template <typename T>
void Deque<T>::move_forward(std::pair<size_t, size_t>& indexes) {
  if (indexes.second + 1 == kBucketSize) {
    indexes.second = 0;
    ++indexes.first;
  } else {
    ++indexes.second;
  }
}
template <typename T>
void Deque<T>::move_back(std::pair<size_t, size_t>& indexes) {
  if (static_cast<int32_t>(indexes.second) - 1 < 0) {
    indexes.second = kBucketSize - 1;
    --indexes.first;
  } else {
    --indexes.second;
  }
}
template <typename T>
void Deque<T>::move_begin_forward() {
  move_forward(begin_);
}
template <typename T>
void Deque<T>::move_begin_back() {
  move_back(begin_);
}
template <typename T>
void Deque<T>::move_end_forward() {
  move_forward(end_);
}
template <typename T>
void Deque<T>::move_end_back() {
  move_back(end_);
}

#endif  // TESTING_REPO_CPP_DEQUE_HPP
