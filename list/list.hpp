//
// Created by esen on 03.04.24.
//

#ifndef TESTING_REPO_CPP_DEQUE_HPP
#define TESTING_REPO_CPP_DEQUE_HPP

#include <iostream>
#include <stdexcept>
#include <vector>

template <typename T, typename Allocator = std::allocator<T>>
class List {
 private:
  class BaseNode;
  class Node;

 public:
  template <bool IsConst = false>
  class Iterator;

  using value_type = T;
  using allocator_type = Allocator;
  using allocator_traits = std::allocator_traits<allocator_type>;
  using node_allocator_type =
      typename allocator_traits::template rebind_alloc<Node>;
  using node_allocator_traits =
      typename allocator_traits::template rebind_traits<Node>;
  using iterator = Iterator<false>;
  using const_iterator = Iterator<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  List();
  List(const size_t& count, const value_type& value,
       const Allocator& allocator = allocator_type());
  explicit List(const size_t& count,
                const Allocator& allocator = allocator_type());
  List(const List& rhs);
  List(std::initializer_list<value_type> initializer_list,
       const Allocator& allocator = allocator_type());
  ~List();

  iterator begin();
  iterator end();
  const_iterator begin() const;
  const_iterator end() const;

  reverse_iterator rbegin();
  reverse_iterator rend();
  const_reverse_iterator rbegin() const;
  const_reverse_iterator rend() const;

  const_iterator cbegin() const;
  const_iterator cend() const;

  const_reverse_iterator crbegin() const;
  const_reverse_iterator crend() const;

  size_t size() const;
  bool empty();
  void push_back_default_value();
  void push_back(const value_type& element_to_create);
  void push_front(const value_type& element_to_create);
  void pop_back();
  void pop_front();
  void insert(iterator element_iterator, const value_type& element_to_create);
  void erase(iterator element_iterator);
  value_type& at(const size_t& index);
  const value_type& at(const size_t& index) const;

  List& operator=(const List& rhs);

  node_allocator_type& get_allocator();
  const node_allocator_type& get_allocator() const;

 private:
  void init_values();
  void link_base_nodes(BaseNode* first_base_node, BaseNode* second_base_node);
  void link_with_fake_node(BaseNode* node);

  void try_construct(Node* created_node, const value_type& element_to_create);
  void try_construct_default_value(Node* created_node);

  void destroy_elements(BaseNode* base_node_start, BaseNode* base_node_end);
  void destroy_all_elements();
  void deallocate_nodes(BaseNode* base_node_start, BaseNode* base_node_end);
  void deallocate_all_nodes();
  void destruct_list();

  BaseNode fake_node_;
  allocator_type allocator_;
  node_allocator_type node_allocator_;
  BaseNode* begin_ = &fake_node_;
  size_t size_ = 0;
};

template <typename T, typename Allocator>
template <bool IsConst>
class List<T, Allocator>::Iterator {
 public:
  using iterator_category = std::bidirectional_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using cond_base_node_type =
      std::conditional_t<IsConst, const BaseNode, BaseNode>;
  using base_node_type = cond_base_node_type;
  using base_node_pointer = cond_base_node_type*;
  using base_node_reference = cond_base_node_type&;
  using cond_node_type = std::conditional_t<IsConst, const Node, Node>;
  using node_type = cond_node_type;
  using node_pointer = cond_node_type*;
  using node_reference = cond_node_type&;
  using cond_type = std::conditional_t<IsConst, const T, T>;
  using value_type = cond_type;
  using pointer = cond_type*;
  using reference = cond_type&;

  Iterator(base_node_pointer base_node) : base_node(base_node) {}
  Iterator(const Iterator&) = default;

  operator Iterator<true>() const { return Iterator<true>(base_node); }

  difference_type operator-(const Iterator& rhs) const {
    base_node_pointer lhs_base_node = this->base_node;
    base_node_pointer rhs_base_node = rhs.base_node;
    difference_type distance = 0;
    while (lhs_base_node != rhs_base_node &&
           lhs_base_node->next_node != lhs_base_node) {
      lhs_base_node = lhs_base_node->next_node;
      ++distance;
    }
    if (lhs_base_node->next_node != lhs_base_node) {
      return distance;
    }

    distance = 0;
    lhs_base_node = this->base_node;
    while (lhs_base_node != rhs_base_node &&
           lhs_base_node->previous_node != nullptr) {
      lhs_base_node = lhs_base_node->previous_node;
      --distance;
    }
    return distance;
  }
  bool operator==(const Iterator& rhs) const {
    return this->base_node == rhs.base_node;
  }
  bool operator!=(const Iterator& rhs) const {
    return this->base_node != rhs.base_node;
  }
  Iterator& operator++() {
    move_forward(1);
    return *this;
  }
  Iterator& operator--() {
    move_back(1);
    return *this;
  }
  Iterator operator--(int) {
    Iterator tmp = *this;
    move_back(1);
    return tmp;
  }
  Iterator operator++(int) {
    Iterator tmp = *this;
    move_forward(1);
    return tmp;
  }
  Iterator& operator+=(const difference_type& n) {
    move_forward(n);
    return *this;
  }
  Iterator& operator-=(const difference_type& n) {
    move_back(n);
    return *this;
  }
  Iterator operator+(const difference_type& n) const {
    Iterator tmp = *this;
    tmp.move_forward(n);
    return tmp;
  }
  Iterator operator-(const difference_type& n) const {
    Iterator tmp = *this;
    tmp.move_back(n);
    return tmp;
  }

  reference operator*() const {
    return static_cast<node_pointer>(base_node)->value;
  }
  pointer operator->() const {
    return &(static_cast<node_pointer>(base_node)->value);
  }
  Iterator& operator=(const Iterator&) = default;
  bool operator<(const Iterator& rhs) { return *this - rhs < 0; }
  bool operator>(const Iterator& rhs) { return *this - rhs > 0; }
  bool operator<=(const Iterator& rhs) { return *this - rhs <= 0; }
  bool operator>=(const Iterator& rhs) { return *this - rhs >= 0; }

  void move_forward(size_t delta) {
    for (size_t index = 0; index < delta; ++index) {
      base_node = base_node->next_node;
    }
  }
  void move_back(size_t delta) {
    for (size_t index = 0; index < delta; ++index) {
      base_node = base_node->previous_node;
    }
  }

  base_node_pointer base_node = nullptr;
};

template <typename T, typename Allocator>
List<T, Allocator>::List() {
  init_values();
}
template <typename T, typename Allocator>
List<T, Allocator>::List(const size_t& count, const value_type& value,
                         const Allocator& allocator)
    : allocator_(allocator) {
  init_values();
  for (size_t index = 0; index < count; ++index) {
    push_back(value);
  }
}
template <typename T, typename Allocator>
List<T, Allocator>::List(const size_t& count, const Allocator& allocator)
    : allocator_(allocator) {
  init_values();
  try {
    for (size_t index = 0; index < count; ++index) {
      push_back_default_value();
    }
  } catch (...) {
    destruct_list();
    throw;
  }
}
template <typename T, typename Allocator>
List<T, Allocator>::List(const List& rhs)
    : node_allocator_(
          node_allocator_traits::select_on_container_copy_construction(
              rhs.get_allocator())) {
  init_values();
  size_t index = 0;
  try {
    for (const value_type& element : rhs) {
      push_back(element);
    }
  } catch (...) {
    destruct_list();

    init_values();
    throw;
  }
}
template <typename T, typename Allocator>
List<T, Allocator>::List(std::initializer_list<value_type> initializer_list,
                         const Allocator& allocator)
    : allocator_(allocator) {
  init_values();
  for (const value_type& element : initializer_list) {
    push_back(element);
  }
}
template <typename T, typename Allocator>
List<T, Allocator>::~List() {
  destruct_list();
}
template <typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
  return iterator(begin_);
}
template <typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
  return iterator(&fake_node_);
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
  return const_iterator(begin_);
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
  return const_iterator(&fake_node_);
}
template <typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
  return reverse_iterator(end());
}
template <typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
  return reverse_iterator(begin());
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin()
    const {
  return const_reverse_iterator(end());
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend()
    const {
  return const_reverse_iterator(begin());
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
  return const_iterator(begin_);
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
  return const_iterator(&fake_node_);
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator
List<T, Allocator>::crbegin() const {
  return const_reverse_iterator(end());
}
template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend()
    const {
  return const_reverse_iterator(begin());
}
template <typename T, typename Allocator>
size_t List<T, Allocator>::size() const {
  return size_;
}
template <typename T, typename Allocator>
bool List<T, Allocator>::empty() {
  return begin_ == &fake_node_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::push_back_default_value() {
  Node* created_node = node_allocator_traits::allocate(node_allocator_, 1);
  try {
    try_construct_default_value(created_node);
  } catch (...) {
    throw;
  }
  if (empty()) {
    link_with_fake_node(static_cast<BaseNode*>(created_node));
    begin_ = created_node;
  } else {
    link_base_nodes(fake_node_.previous_node,
                    static_cast<BaseNode*>(created_node));
    link_base_nodes(static_cast<BaseNode*>(created_node), &fake_node_);
  }
  ++size_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::push_back(const value_type& element_to_create) {
  Node* created_node = node_allocator_traits::allocate(node_allocator_, 1);
  try {
    try_construct(created_node, element_to_create);
  } catch (...) {
    throw;
  }
  if (this->empty()) {
    link_with_fake_node(static_cast<BaseNode*>(created_node));
    begin_ = created_node;
  } else {
    link_base_nodes(fake_node_.previous_node,
                    static_cast<BaseNode*>(created_node));
    link_base_nodes(static_cast<BaseNode*>(created_node), &fake_node_);
  }
  ++size_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::push_front(const value_type& element_to_create) {
  Node* created_node = node_allocator_traits::allocate(node_allocator_, 1);
  try {
    try_construct(created_node, element_to_create);
  } catch (...) {
    throw;
  }
  link_base_nodes(static_cast<BaseNode*>(created_node), begin_);
  begin_ = static_cast<BaseNode*>(created_node);
  ++size_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
  fake_node_.previous_node = fake_node_.previous_node->previous_node;
  node_allocator_traits::destroy(
      node_allocator_,
      &(static_cast<Node*>(fake_node_.previous_node->next_node)->value));
  node_allocator_traits::deallocate(
      node_allocator_, static_cast<Node*>(fake_node_.previous_node->next_node),
      1);
  fake_node_.previous_node->next_node = &fake_node_;
  --size_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
  begin_ = begin_->next_node;
  node_allocator_traits::destroy(
      node_allocator_, &(static_cast<Node*>(begin_->previous_node)->value));
  node_allocator_traits::deallocate(
      node_allocator_, static_cast<Node*>(begin_->previous_node), 1);
  begin_->previous_node = nullptr;
  --size_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::insert(List::iterator element_iterator,
                                const value_type& element_to_create) {
  Node* created_node = node_allocator_traits::allocate(node_allocator_, 1);
  try {
    try_construct(created_node, element_to_create);
  } catch (...) {
    throw;
  }
  if (this->empty()) {
    link_with_fake_node(static_cast<BaseNode*>(created_node));
    begin_ = created_node;
  } else {
    link_base_nodes(element_iterator.base_node->previous_node,
                    static_cast<BaseNode*>(created_node));
    link_base_nodes(static_cast<BaseNode*>(created_node),
                    element_iterator.base_node);
  }
  ++size_;
}
template <typename T, typename Allocator>
void List<T, Allocator>::erase(List::iterator element_iterator) {
  link_base_nodes(element_iterator.base_node->previous_node,
                  element_iterator.base_node->next_node);
  node_allocator_traits::destroy(
      node_allocator_,
      &(static_cast<Node*>(element_iterator.base_node)->value));
  node_allocator_traits::deallocate(
      node_allocator_, static_cast<Node*>(element_iterator.base_node), 1);
  --size_;
}
template <typename T, typename Allocator>
typename List<T, Allocator>::value_type& List<T, Allocator>::at(
    const size_t& index) {
  if (index > (size_ - 1)) {
    throw std::out_of_range("out of range");
  }
  return (*this)[index];
}
template <typename T, typename Allocator>
const typename List<T, Allocator>::value_type& List<T, Allocator>::at(
    const size_t& index) const {
  if (index > (size_ - 1)) {
    throw std::out_of_range("out of range");
  }
  return (*this)[index];
}
template <typename T, typename Allocator>
List<T, Allocator>& List<T, Allocator>::operator=(const List& rhs) {
  if (this == &rhs) {
    return *this;
  }

  if (node_allocator_traits::propagate_on_container_copy_assignment::value) {
    allocator_ = rhs.allocator_;
    node_allocator_ = rhs.node_allocator_;

    destruct_list();
    init_values();
  }

  iterator lhs_current = this->begin();
  const_iterator rhs_current = rhs.begin();
  try {
    for (; lhs_current != this->end() && rhs_current != rhs.end();
         ++lhs_current, ++rhs_current) {
      *lhs_current = *rhs_current;
    }
    if (rhs_current != rhs.end()) {
      for (; rhs_current != rhs.end(); ++rhs_current, ++lhs_current) {
        insert(lhs_current, *rhs_current);
      }
    } else {
      for (; lhs_current != this->end();) {
        erase(lhs_current++);
      }
    }
  } catch (...) {
    throw;
  }
  return *this;
}
template <typename T, typename Allocator>
typename List<T, Allocator>::node_allocator_type&
List<T, Allocator>::get_allocator() {
  return node_allocator_;
}
template <typename T, typename Allocator>
const typename List<T, Allocator>::node_allocator_type&
List<T, Allocator>::get_allocator() const {
  return node_allocator_;
}

template <typename T, typename Allocator>
class List<T, Allocator>::BaseNode {
 public:
  BaseNode() = default;
  BaseNode(BaseNode* next_node, BaseNode* previous_node)
      : next_node(next_node), previous_node(previous_node) {}

  BaseNode* next_node = nullptr;
  BaseNode* previous_node = nullptr;
};

template <typename T, typename Allocator>
class List<T, Allocator>::Node : public BaseNode {
 public:
  Node(Node* next_node, Node* previous_node)
      : BaseNode(next_node, previous_node) {}
  Node(Node* next_node, Node* previous_node, value_type& value)
      : BaseNode(next_node, previous_node), value(value) {}

  value_type value;
};

template <typename T, typename Allocator>
void List<T, Allocator>::init_values() {
  begin_ = &fake_node_;
  fake_node_.next_node = &fake_node_;
  fake_node_.previous_node = &fake_node_;
  size_ = 0;
}
template <typename T, typename Allocator>
void List<T, Allocator>::link_base_nodes(List::BaseNode* first_base_node,
                                         List::BaseNode* second_base_node) {
  first_base_node->next_node = second_base_node;
  second_base_node->previous_node = first_base_node;
}
template <typename T, typename Allocator>
void List<T, Allocator>::link_with_fake_node(List::BaseNode* node) {
  node->next_node = &fake_node_;
  fake_node_.previous_node = node;
}
template <typename T, typename Allocator>
void List<T, Allocator>::try_construct(List::Node* created_node,
                                       const value_type& element_to_create) {
  try {
    node_allocator_traits::construct(node_allocator_, &(created_node->value),
                                     std::move(element_to_create));
  } catch (...) {
    node_allocator_traits::deallocate(node_allocator_, created_node, 1);
    throw;
  }
}
template <typename T, typename Allocator>
void List<T, Allocator>::try_construct_default_value(List::Node* created_node) {
  try {
    node_allocator_traits::construct(node_allocator_, &(created_node->value));
  } catch (...) {
    node_allocator_traits::deallocate(node_allocator_, created_node, 1);
    throw;
  }
}
template <typename T, typename Allocator>
void List<T, Allocator>::destroy_elements(List::BaseNode* base_node_start,
                                          List::BaseNode* base_node_end) {
  for (; base_node_start != base_node_end;
       base_node_start = base_node_start->next_node) {
    node_allocator_traits::destroy(
        node_allocator_, &(static_cast<Node*>(base_node_start)->value));
  }
}
template <typename T, typename Allocator>
void List<T, Allocator>::destroy_all_elements() {
  destroy_elements(begin_, &fake_node_);

  size_ = 0;
}
template <typename T, typename Allocator>
void List<T, Allocator>::deallocate_nodes(List::BaseNode* base_node_start,
                                          List::BaseNode* base_node_end) {
  BaseNode* next_node = base_node_start->next_node;
  for (; base_node_start != base_node_end;
       base_node_start = next_node, next_node = base_node_start->next_node) {
    node_allocator_traits::deallocate(node_allocator_,
                                      static_cast<Node*>(base_node_start), 1);
  }
}
template <typename T, typename Allocator>
void List<T, Allocator>::deallocate_all_nodes() {
  deallocate_nodes(begin_, &fake_node_);
  size_ = 0;
}
template <typename T, typename Allocator>
void List<T, Allocator>::destruct_list() {
  if (size_ == 0) {
    return;
  }
  destroy_all_elements();
  deallocate_all_nodes();
}

#endif  // TESTING_REPO_CPP_DEQUE_HPP
