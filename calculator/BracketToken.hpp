//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_BRACKETTOKEN_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_BRACKETTOKEN_HPP

#include "AbstractToken.hpp"

class BracketToken : public AbstractToken {
 public:
  BracketToken(const std::string& bracket_token_string)
      : AbstractToken(bracket_token_string){};
  bool IsOpening() const { return GetStringToken() == "("; };
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_BRACKETTOKEN_HPP
