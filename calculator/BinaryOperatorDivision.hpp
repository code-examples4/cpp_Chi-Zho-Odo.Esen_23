//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORDIVISION_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORDIVISION_HPP

#include <cassert>

#include "OperatorToken.hpp"

template <typename T>
class BinaryOperatorDivision : public OperatorToken<T, true> {
 public:
  BinaryOperatorDivision(const std::string& binary_operator_division)
      : OperatorToken<T, true>(binary_operator_division) {}
  T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) {
    return lhs->GetValue() / rhs->GetValue();
  }
  T Calculate(OperandToken<T>* value) {
    assert(value);
    assert(false);
  };
  ~BinaryOperatorDivision() = default;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORDIVISION_HPP
