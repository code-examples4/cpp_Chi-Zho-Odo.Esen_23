//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_UNARYOPERATORMINUS_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_UNARYOPERATORMINUS_HPP

#include <cassert>

#include "OperatorToken.hpp"

template <typename T>
class UnaryOperatorMinus : public OperatorToken<T, false> {
 public:
  UnaryOperatorMinus(const std::string& unary_operator_minus_token)
      : OperatorToken<T, false>(unary_operator_minus_token) {}
  T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) {
    assert(lhs);
    assert(rhs);
    assert(false);
  };
  T Calculate(OperandToken<T>* value) {
    T result = value->GetValue();
    return -result;
  }
  ~UnaryOperatorMinus() = default;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_UNARYOPERATORMINUS_HPP
