//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORPLUS_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORPLUS_HPP

#include <cassert>

#include "OperatorToken.hpp"

template <typename T>
class BinaryOperatorPlus : public OperatorToken<T, true> {
 public:
  BinaryOperatorPlus(const std::string& binary_operator_plus_token)
      : OperatorToken<T, true>(binary_operator_plus_token) {}
  T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) {
    return lhs->GetValue() + rhs->GetValue();
  }
  T Calculate(OperandToken<T>* value) {
    assert(value);
    assert(false);
  };
  ~BinaryOperatorPlus() = default;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORPLUS_HPP
