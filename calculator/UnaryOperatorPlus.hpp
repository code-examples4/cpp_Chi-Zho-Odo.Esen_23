//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_UNARYOPERATORPLUS_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_UNARYOPERATORPLUS_HPP

#include <cassert>

#include "OperatorToken.hpp"

template <typename T>
class UnaryOperatorPlus : public OperatorToken<T, false> {
 public:
  UnaryOperatorPlus(const std::string& unary_operator_plus_token)
      : OperatorToken<T, false>(unary_operator_plus_token) {}
  T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) {
    assert(lhs);
    assert(rhs);
    assert(false);
  };
  T Calculate(OperandToken<T>* value) { return value->GetValue(); }
  ~UnaryOperatorPlus() = default;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_UNARYOPERATORPLUS_HPP
