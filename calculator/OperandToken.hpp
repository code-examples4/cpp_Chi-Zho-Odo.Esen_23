//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_OPERANDTOKEN_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_OPERANDTOKEN_HPP

#include <sstream>
#include <string>

#include "AbstractToken.hpp"

template <typename T>
class OperandToken : public AbstractToken {
 public:
  OperandToken(const std::string& operand_token_string)
      : AbstractToken(operand_token_string) {
    std::stringstream stringstream;
    stringstream << operand_token_string;
    stringstream >> value_;
  }
  OperandToken(const T& value)
      : AbstractToken(MakeStringFromValue(value)), value_(value) {}
  const T& GetValue() { return value_; }

 private:
  std::string MakeStringFromValue(const T& value) {
    std::stringstream stringstream;
    stringstream << value;
    return stringstream.str();
  }

  T value_;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_OPERANDTOKEN_HPP
