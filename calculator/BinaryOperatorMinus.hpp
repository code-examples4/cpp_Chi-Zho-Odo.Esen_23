//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORMINUS_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORMINUS_HPP

#include <cassert>

#include "OperatorToken.hpp"

template <typename T>
class BinaryOperatorMinus : public OperatorToken<T, true> {
 public:
  BinaryOperatorMinus(const std::string& binary_operator_minus_token)
      : OperatorToken<T, true>(binary_operator_minus_token) {}
  T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) {
    return lhs->GetValue() - rhs->GetValue();
  }
  T Calculate(OperandToken<T>* value) {
    assert(value);
    assert(false);
  };
  ~BinaryOperatorMinus() = default;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORMINUS_HPP
