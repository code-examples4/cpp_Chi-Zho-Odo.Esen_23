//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_EXPRINPOLISHNOTATION_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_EXPRINPOLISHNOTATION_HPP

#include <algorithm>
#include <map>
#include <stack>
#include <string>
#include <vector>

#include "BinaryOperatorDivision.hpp"
#include "BinaryOperatorMinus.hpp"
#include "BinaryOperatorPlus.hpp"
#include "BinaryOperatorProduct.hpp"
#include "BracketToken.hpp"
#include "UnaryOperatorMinus.hpp"
#include "UnaryOperatorPlus.hpp"

template <typename T>
class ExprInPolishNotation {
 public:
  ExprInPolishNotation(const std::string& expression_in_infix_notation) {
    std::stack<AbstractToken*> elements_stack;
    char character;
    for (int32_t index = expression_in_infix_notation.size() - 1; index >= 0;
         --index) {
      character = expression_in_infix_notation[index];
      if (std::isdigit(character) != 0) {
        HandleValue(expression_in_infix_notation, index);
      } else if (IsOperator(character)) {
        HandleOperator(expression_in_infix_notation, elements_stack, character,
                       index);
      } else if (IsBracket(character)) {
        HandleBracket(elements_stack, character);
      }
    }
    while (!elements_stack.empty()) {
      expression_tokens_.push_back(elements_stack.top());
      elements_stack.pop();
    }
  }
  ~ExprInPolishNotation() {
    for (AbstractToken* token : expression_tokens_) {
      delete token;
    }
  }

  const std::vector<AbstractToken*>& GetTokens() { return expression_tokens_; }

  void HandleValue(const std::string& expression_in_infix_notation,
                   int32_t& index) {
    OperandToken<T>* element = new OperandToken<T>(
        GetValueString(expression_in_infix_notation, index));
    expression_tokens_.push_back(element);
  }
  void HandleOperator(const std::string& expression_in_infix_notation,
                      std::stack<AbstractToken*>& elements_stack,
                      char character, int32_t& index) {
    AbstractToken* element =
        CreateOperator(character, index, expression_in_infix_notation);
    while ((!elements_stack.empty())) {
      if (GetOperatorPriority(elements_stack.top()) >=
          GetOperatorPriority(element)) {
        expression_tokens_.push_back(elements_stack.top());
        elements_stack.pop();
        continue;
      }
      break;
    }
    elements_stack.push(element);
  }
  void HandleBracket(std::stack<AbstractToken*>& elements_stack,
                     char character) {
    BracketToken* element = new BracketToken(std::string(1, character));
    if (element->IsOpening()) {
      expression_tokens_.push_back(element);
      while (!elements_stack.empty()) {
        expression_tokens_.push_back(elements_stack.top());
        if (GetOperatorPriority(elements_stack.top()) == 0) {
          elements_stack.pop();
          break;
        }
        elements_stack.pop();
      }
    } else {
      elements_stack.push(element);
    }
  }

  static bool IsOperator(const std::string& character) {
    return (character == "+") || (character == "-") || (character == "/") ||
           (character == "*");
  }
  static bool IsOperator(char character) {
    return (character == '+') || (character == '-') || (character == '/') ||
           (character == '*');
  }

  static bool IsBracket(const std::string& character) {
    return (character == "(") || (character == ")");
  }
  static bool IsBracket(char character) {
    return (character == '(') || (character == ')');
  }

  static bool IsOpeningBracket(const std::string& character) {
    return (character == "(");
  }
  static bool IsOpeningBracket(char character) { return (character == '('); }

  static std::string GetValueString(
      const std::string& expression_in_infix_notation, int32_t& index) {
    std::string value_string;
    for (; index >= 0; --index) {
      char character = expression_in_infix_notation[index];
      if ((isdigit(character) != 0) || (character == '.')) {
        value_string += character;
      } else {
        ++index;
        break;
      }
    }
    std::reverse(value_string.begin(), value_string.end());
    return value_string;
  }

  static bool IsBinaryOperator(AbstractToken* element) {
    return static_cast<bool>(dynamic_cast<OperatorToken<T, true>*>(element));
  }

  int32_t GetOperatorPriority(AbstractToken* element) {
    if (dynamic_cast<UnaryOperatorPlus<T>*>(element)) {
      return kOperatorsPriority.at("unary +");
    }
    if (dynamic_cast<UnaryOperatorMinus<T>*>(element)) {
      return kOperatorsPriority.at("unary -");
    }
    if (dynamic_cast<BinaryOperatorProduct<T>*>(element)) {
      return kOperatorsPriority.at("binary *");
    }
    if (dynamic_cast<BinaryOperatorDivision<T>*>(element)) {
      return kOperatorsPriority.at("binary /");
    }
    if (dynamic_cast<BinaryOperatorPlus<T>*>(element)) {
      return kOperatorsPriority.at("binary +");
    }
    if (dynamic_cast<BinaryOperatorMinus<T>*>(element)) {
      return kOperatorsPriority.at("binary -");
    }
    return 0;
  }

  AbstractToken* CreateUnaryOperator(char character) {
    if (character == '+') {
      return new UnaryOperatorPlus<T>(std::string(1, character));
    }
    if (character == '-') {
      return new UnaryOperatorMinus<T>(std::string(1, character));
    }
    return nullptr;
  }
  AbstractToken* CreateBinaryOperator(char character) {
    if (character == '*') {
      return new BinaryOperatorProduct<T>(std::string(1, character));
    }
    if (character == '/') {
      return new BinaryOperatorDivision<T>(std::string(1, character));
    }
    if (character == '+') {
      return new BinaryOperatorPlus<T>(std::string(1, character));
    }
    if (character == '-') {
      return new BinaryOperatorMinus<T>(std::string(1, character));
    }
    return nullptr;
  }
  AbstractToken* CreateOperator(
      char character, int32_t index,
      const std::string& expression_in_infix_notation) {
    char next_character = expression_in_infix_notation[index - 1];
    while (isspace(next_character) != 0 && index >= 0) {
      --index;
      next_character = expression_in_infix_notation[index - 1];
    }
    if (IsOperator(next_character) || IsOpeningBracket(next_character) ||
        index < 0) {
      return CreateUnaryOperator(character);
    }
    return CreateBinaryOperator(character);
  }

 private:
  static const std::map<std::string, int32_t> kOperatorsPriority;
  std::vector<AbstractToken*> expression_tokens_;
};
template <typename T>
const std::map<std::string, int32_t>
    ExprInPolishNotation<T>::kOperatorsPriority = {
        {"unary +", 3},  {"unary -", 3},  {"binary /", 2},
        {"binary *", 2}, {"binary +", 1}, {"binary -", 1}};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_EXPRINPOLISHNOTATION_HPP
