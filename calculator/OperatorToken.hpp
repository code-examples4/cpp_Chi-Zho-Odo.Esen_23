//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_OPERATORTOKEN_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_OPERATORTOKEN_HPP

#include "OperandToken.hpp"

template <typename T, bool IsBinaryFlag>
class OperatorToken : public AbstractToken {
 public:
  OperatorToken(const std::string& operator_token_string)
      : AbstractToken(operator_token_string) {}
  bool IsBinary() { return IsBinaryFlag; }
  virtual T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) = 0;
  virtual T Calculate(OperandToken<T>* value) = 0;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_OPERATORTOKEN_HPP
