//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORPRODUCT_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORPRODUCT_HPP

#include <cassert>

#include "OperatorToken.hpp"

template <typename T>
class BinaryOperatorProduct : public OperatorToken<T, true> {
 public:
  BinaryOperatorProduct(const std::string& binary_operator_product)
      : OperatorToken<T, true>(binary_operator_product) {}
  T Calculate(OperandToken<T>* lhs, OperandToken<T>* rhs) {
    return lhs->GetValue() * rhs->GetValue();
  }
  T Calculate(OperandToken<T>* value) {
    assert(value);
    assert(false);
  };
  ~BinaryOperatorProduct() = default;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_BINARYOPERATORPRODUCT_HPP
