//
// Created by esen on 15.12.23.
//

#ifndef CPP_CHI_ZHO_ODO_ESEN_23_ABSTRACTTOKEN_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_ABSTRACTTOKEN_HPP

#include <string>

class AbstractToken {
 public:
  const std::string& GetStringToken() const { return token_string_; }
  virtual ~AbstractToken() = default;

 protected:
  AbstractToken(const std::string& token_string)
      : token_string_(token_string) {}

 private:
  std::string token_string_;
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_ABSTRACTTOKEN_HPP
