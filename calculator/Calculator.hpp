#ifndef CPP_CHI_ZHO_ODO_ESEN_23_CALCULATOR_HPP
#define CPP_CHI_ZHO_ODO_ESEN_23_CALCULATOR_HPP

#include <stack>

#include "AbstractToken.hpp"
#include "BracketToken.hpp"
#include "ExprInPolishNotation.hpp"
#include "OperandToken.hpp"
#include "OperatorToken.hpp"

template <typename T>
class Calculator {
 public:
  struct InvalidExpr : std::exception {
   public:
    [[nodiscard]] const char* what() const noexcept override {
      return "Invalid expression!";
    }
  };

  static T CalculateExpr(const std::string& expression) {
    ExprInPolishNotation<T> expression_in_polish_notation(expression);
    std::vector<AbstractToken*> tokens_stacklike_vector =
        expression_in_polish_notation.GetTokens();
    CheckValidity(tokens_stacklike_vector);
    int32_t index = static_cast<int32_t>(tokens_stacklike_vector.size()) - 1;
    return CalculatePartOfExpression(tokens_stacklike_vector, index);
  }
  static T CalculatePartOfExpression(
      const std::vector<AbstractToken*>& tokens_stacklike_vector,
      int32_t& index) {
    if (ExprInPolishNotation<T>::IsOperator(
            tokens_stacklike_vector[index]->GetStringToken())) {
      if (ExprInPolishNotation<T>::IsBinaryOperator(
              tokens_stacklike_vector[index])) {
        OperatorToken<T, true>* element = dynamic_cast<OperatorToken<T, true>*>(
            tokens_stacklike_vector[index]);
        --index;
        OperandToken<T> lhs(
            CalculatePartOfExpression(tokens_stacklike_vector, index));
        OperandToken<T> rhs(
            CalculatePartOfExpression(tokens_stacklike_vector, index));
        return element->Calculate(&lhs, &rhs);
      }
      OperatorToken<T, false>* element = dynamic_cast<OperatorToken<T, false>*>(
          tokens_stacklike_vector[index]);
      --index;
      OperandToken<T> value(
          CalculatePartOfExpression(tokens_stacklike_vector, index));
      return element->Calculate(&value);
    }
    if (dynamic_cast<BracketToken*>(tokens_stacklike_vector[index]) !=
        nullptr) {
      --index;
      return CalculatePartOfExpression(tokens_stacklike_vector, index);
    }
    OperandToken<T>* element =
        dynamic_cast<OperandToken<T>*>(tokens_stacklike_vector[index]);
    --index;
    return element->GetValue();
  }

  static void CheckValidity(std::vector<AbstractToken*> expression_tokens) {
    int32_t index = expression_tokens.size() - 1;
    int32_t brackets_coefficient = 0;
    if (!CheckPartValidity(expression_tokens, index, brackets_coefficient) ||
        index != -1 || brackets_coefficient != 0) {
      throw InvalidExpr{};
    }
  }
  static bool CheckPartValidity(std::vector<AbstractToken*> expression_tokens,
                                int32_t& index, int32_t& brackets_coefficient) {
    if (index < 0 || brackets_coefficient < 0) {
      return false;
    }
    --index;
    if (ExprInPolishNotation<T>::IsOperator(
            expression_tokens[index + 1]->GetStringToken())) {
      if (ExprInPolishNotation<T>::IsBinaryOperator(
              expression_tokens[index + 1])) {
        bool lhs =
            CheckPartValidity(expression_tokens, index, brackets_coefficient);
        bool rhs =
            CheckPartValidity(expression_tokens, index, brackets_coefficient);
        return lhs && rhs;
      }
      return CheckPartValidity(expression_tokens, index, brackets_coefficient);
    }
    if (BracketToken* element =
            dynamic_cast<BracketToken*>(expression_tokens[index + 1])) {
      if (element->IsOpening()) {
        --brackets_coefficient;
        return CheckPartValidity(expression_tokens, index,
                                 brackets_coefficient);
      }
      ++brackets_coefficient;
      return CheckPartValidity(expression_tokens, index, brackets_coefficient);
    }
    return true;
  };
};

#endif  // CPP_CHI_ZHO_ODO_ESEN_23_CALCULATOR_HPP
